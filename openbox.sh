#!/bin/bash

clear
echo "          Mi instalación personal de Openbox para Debian 9 / 10 / 11"
echo "          ------- ----"
source ./config/bin/script.library
user_id
[ $USER_ID -ne 0 ] && exit 1	# Nos salimos porque no eres root

debian_version

read -p "¿Quieres actualizar/corregir el sources.list de Debian? (S/N)" UNO
echo ""
read -p "¿Quieres instalar/ACTUALIZAR EL SISTEMA con las aplicaciones que he seleccionado para tí?: (S/N)" DOS
echo ""
read -p "¿Quieres intalar gtkDialog, indicator-keylock, Sublime Text y master-pdf-editor 4 ? (S/N): " TRES
echo ""
read -p "¿Quieres instalar/actualizar los ARCHIVOS DE CONFIGURACIÓN de tu sistema?: (S/N)" CUATRO
echo ""


if [[ "$UNO" = s || "$UNO" = S || "$UNO" = Y || "$UNO" = y ]]
then
	source ./openbox_01.sub 	# Instalamos/Actualizamos el source.list adecuado
fi

if [[ "$DOS" = s || "$DOS" = S || "$DOS" = Y || "$DOS" = y ]]
then
	source ./openbox_02.sub 	# Instalamos los paquete que hemos configurado
fi

if [[ "$TRES" = s || "$TRES" = S || "$TRES" = Y || "$TRES" = y ]]
then
	source ./openbox_03.sub 	# INSTALAMOS SUBLIMETEXT, INDICATOR KEYLOCK Y GTKDIALOG
fi

if [[ "$CUATRO" = s || "$CUATRO" = S || "$CUATRO" = Y || "$CUATRO" = y ]]
then
	source ./openbox_04.sub 	# COPIAMOS LOS ARCHIVOS DE CONFIGURACIÓN A SU DESTINO CON SUS PERMISOS
fi

echo '6 - Fin del script'

####NOTAS FINALES######
# nano /usr/lib/x86_64-linux-gnu/openbox-autostart comentar la última línea para que no ejecute las aplicaciones del /etc/xdg/autostart
# nano /usr/lib/i386-linux-gnu/openbox-autostart
