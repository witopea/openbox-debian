# OpenboxDebian

Bash Script para instalar Openbox en tu Debian 9, 10 u 11 ya configurado, con un panel de control
muy sencillo desde el que puedes cambiar los temas de iconos, temas GTK, temas de Openbox, etc...
![](pantallazo.png)
Yo lo uso para mis instalaciones de Debian desde el netInstall (instalación limpia de Debian sin entorno gráfico alguno).
Después de ejecutar este script tengo mi Debian con openBox totalmente configurado y operativo.

El panel de control lo tomé de SalentOS y lo personalicé a mi gusto.

Este bash script está hecho para mi uso personal, lo hago público por si alguien lo quiere usar y personalizar a su gusto y por supuesto al igual que el mensaje típico de Debian:

`Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY`

Podemos decir:

`This bash script comes with ABSOLUTELY NO WARRANTY`

# Instalación
Puedes clonar este repositorio `git clone https://gitlab.com/witopea/openbox-debian.git` o bajártelo como archivo comprimido.
Una vez lo tengas en tu equipo busca el archivo openbox.sh, ábrelo con un editor de texto y revisa su contenido
por si quieres modificarlo. Si quieres ejecutarlo te vas a la consola y escribes:

`sudo ./openbox.sh`

El script te hace cuatro preguntas:

**1.- ¿Quieres actualizar/corregir el sources.list de Debian?: (S/N)"**

Ahí recomiendo SI, porque pone el sources.list adecuado. En el caso de Debian Testing (la próxima Debian 11) he puesto unos repositorios congelados en Diciembre de 2020 para que no ande todo el día actualizando.

**2.- ¿Quieres instalar/ACTUALIZAR EL SISTEMA con las aplicaciones que he seleccionado para tí?: (S/N)"**

Ahí recomiendo SI, tarda un rato y te instala las aplicaciones.

**3.- ¿Quieres intalar gtkialog, indicator-keylock, Sublime Text y master-pdf-editor 4 ? (S/N):**

- **gtkdialog** es una librería que usa el panel de control. Es importante que la instales.
- **indicator-keylock**, sirve para mostrar si la tecla Block Mayúscula está o no activada.
- **Sublime-text**, es un excelente editor de texto que uso.
- **master-pdf-editor 4** un fantástico editor de pdf. Uso la versión 4 porque en la siguiente versión le han puesto un texto de fondo a los pdfs que editas.

**4.- ¿Quieres instalar/actualizar los ARCHIVOS DE CONFIGURACIÓN de tu sistema?: (S/N)**

Configura las aplicaciones que has instalado en los pasos anteriores.

Espero que te sea útil.

Cualquier sugerencia puedes hacermela llegar a través de mi correo witopea@gmail.com
