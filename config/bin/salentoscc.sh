#!/bin/bash
# Acknowledgements: Salentos. Original script by KDulcimer of TinyMe. http://tinyme.mypclinuxos.com
###################################################################################################################################

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=salentoscc.sh
ED1=$EDIT
TERM=lxterminal


export ControlCenter=$(cat <<End_of_Text
<window title="Panel de Control" window-position="1"  icon-name="emblem-system-symbolic.symbolic" decorated="true" allow-grow="false">
<vbox>



                 <pixmap><height>100</height><width>100</width>
                 <input file>/usr/share/ies/dock/icons/debian.svg</input>
                 </pixmap>
                 <text use-markup="true" width-chars="50">
                 <label>"<b>`gettext $"Panel de Control"`</b>"</label>
                 </text>



<notebook tab-pos="0" labels="Apariencia|Openbox|Hardware">

	<hbox homogeneous="true">
	  <vbox>
		<hbox>
		  <button tooltip-text="`gettext $"Elige Fondo de escritorio"`" height-request="60" width-request="60"  stock-icon-size="6" relief="2">
          <input file icon="preferences-desktop-wallpaper"></input><height>48</height><width>48</width>
          <action>nitrogen &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Fondo de escritorio"`"</label>
		  </text>
		</hbox>
<hbox>
<button tooltip-text="`gettext $"Salvapantallas"`" height-request="60" width-request="60"  stock-icon-size="6" relief="2">
          <input file icon="preferences-desktop-screensaver"></input><height>48</height><width>48</width>
          <action>xscreensaver-demo</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Salvapantallas"`"</label>
		  </text>
</hbox>
          </vbox>
          <vbox>
		<hbox>
          <button tooltip-text="`gettext $"Change Gtk2 and Icon Themes"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
          <input file icon="preferences-desktop-theme"></input><height>48</height><width>48</width>
		  <action>/usr/bin/lxappearance &</action>
          </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Apariencia GTK"`"</label>
		  </text>
		</hbox>
	  </vbox>
<vbox>
        <hbox>
		  <button tooltip-text="`gettext $"Openbox Config"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="preferences-system-windows"></input><height>48</height><width>48</width>
		  <action> /usr/bin/obconf &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Apariencia Ventanas"`"</label>
		  </text>
		</hbox>
       <hbox>
           <button tooltip-text="`gettext $"Qt 4 Settings"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="qtconfig-qt4"></input><height>48</height><width>48</width>
		  <action>qtconfig-qt4</action>
           </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Qt 4"`"</label>
		  </text>
		</hbox>
	  </vbox>
	</hbox>
       <hbox  homogeneous="true">
	  <vbox>
	    <hbox>
         <button tooltip-text="`gettext $"Edit rc file"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="preferences-desktop-keyboard-shortcuts"></input><height>48</height><width>48</width>
		  <action>$ED1 ~/.config/openbox/rc.xml &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Edit rc"`"</label>
		  </text>
		</hbox>
		<hbox>
		  <button tooltip-text="`gettext $"Edit autostart"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="system-run"></input><height>48</height><width>48</width>
		  <action>$ED1 ~/.config/openbox/autostart</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Edit autostart"`"</label>
		  </text>
		</hbox>
             </vbox>
	  <vbox>
                <hbox>
		  <button tooltip-text="`gettext $"Edit menu"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="applications-system"></input><height>48</height><width>48</width>
		  <action>obmenu  &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Edit Menu"`"</label>
		  </text>
		</hbox>
	    <hbox>
         <button tooltip-text="`gettext $"Edit Environment file"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="tools-check-spelling-symbolic.symbolic"></input><height>48</height><width>48</width>
		  <action>$ED1 ~/.config/openbox/environment &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Edit Environment"`"</label>
		  </text>
		</hbox>
	  </vbox>
	</hbox>

       <hbox  homogeneous="true">
	  <vbox>
                <hbox>
		  <button tooltip-text="`gettext $"Set Screen Resolution"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="preferences-desktop-display"></input><height>48</height><width>48</width>
		  <action>arandr</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Screen"`"</label>
		  </text>
		</hbox>
        <hbox>
		  <button tooltip-text="`gettext $"Printer"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="printer"></input><height>48</height><width>48</width>
		  <action>system-config-printer</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Printer"`"</label>
		  </text>
		</hbox>
</vbox>

          <vbox>
		<hbox>
		  <button tooltip-text="`gettext $"Power Management"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="ac-adapter"></input><height>48</height><width>48</width>
		  <action>xfce4-power-manager-settings</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Power Management"`"</label>
		  </text>
        </hbox>
<hbox>
		  <button tooltip-text="`gettext $"Adjust Mixer"`" height-request="60" width-request="60" stock-icon-size="6" relief="2" >
		  <input file icon="multimedia-volume-control"></input><height>48</height><width>48</width>
		  <action>pavucontrol &</action>
		  </button>
		  <text use-markup="true" width-chars="12">
		  <label>"`gettext $"Mixer"`"</label>
		  </text>
		</hbox>


 </vbox>
	</hbox>

 </notebook>

<hbox>
<vbox>
    <hbox>
        <button>
          <input file stock="gtk-close"></input>
          <label>"`gettext $"Close"`"</label>
          <action>EXIT:cancel</action>
         </button>
     </hbox>
</vbox>
</hbox>

</vbox>
</window>
End_of_Text
)
gtkdialog --program=ControlCenter
unset ControlCenter
