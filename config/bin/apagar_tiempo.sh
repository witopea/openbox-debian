#!/bin/bash
gxmessage El equipo se apagará en: -center -title "Elige una opción" -font "Sans bold 10" -default "Salir" -buttons "_Salir":1,"_killall":2,"_15 min":3,"30 min":4,"45 min":5,"1h":6,"2h":7,"3h":8,"4h":9,"5h":10,"1m":11
case $? in
  1)
     echo "Exit";;
  2)
     sudo /sbin/shutdown -c;notify-send "Scheduled Shutdown" "The sheduled shutdown has been cancelled." -i /usr/share/ies/dock/icons/shutdown.png -t 10000;;
  3)
     sudo /sbin/shutdown -h +15;notify-send "Scheduled Shutdown" "The computer will turn off in 15 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 13m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  4)
     sudo /sbin/shutdown -h +30;notify-send "Scheduled Shutdown" "The computer will turn off in 30 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 28m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  5)
     sudo /sbin/shutdown -h +45;notify-send "Scheduled Shutdown" "The computer will turn off in 45 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 43m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  6)
     sudo /sbin/shutdown -h +60;notify-send "Scheduled Shutdown" "The computer will turn off in 1 hora." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 58m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  7)
     sudo /sbin/shutdown -h +120;notify-send "Scheduled Shutdown" "The computer will turn off in 2 horas." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 118m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  8)
     sudo /sbin/shutdown -h +180;notify-send "Scheduled Shutdown" "The computer will turn off in 3 horas." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 178m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  9)
     sudo /sbin/shutdown -h +240;notify-send "Scheduled Shutdown" "The computer will turn off in 4 horas." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 238m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  10)
     sudo /sbin/shutdown -h +300;notify-send "Scheduled Shutdown" "The computer will turn off in 5 horas." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 298m;notify-send "Scheduled Shutdown" "The computer will turn off in 2 minutes." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;;
  11)
     sudo /sbin/shutdown -h +1;notify-send "Scheduled Shutdown" "The computer will turn off in 1 minute." -i /usr/share/ies/dock/icons/shutdown.png -t 60000;sleep 50s;notify-send "Scheduled Shutdown" "The computer will turn off in 10 segundos." -i /usr/share/ies/dock/icons/shutdown.png -t 10000;;
esac
