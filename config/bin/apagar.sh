#!/bin/bash
#FUENTE: CREO QUE FUE ESTA;
#https://debianfacil.wordpress.com/2011/09/18/openbox-8-cuadro-de-dialogo-para-apagar-el-ordenador/

if [[ $VIGILAR_CARPETA == 1 ]]  # aquí entra si está activada esa variable de entorno para un script que tengo para sincronizar una carpeta con gDrive
then

  [[ -f "$HOME/.config/bin/driveUP.FLAG" ]] && /usr/bin/notify-send "driveUP" "En ejecución... No se puede apagar el equipo." -i /usr/share/ies/dock/icons/google_drive.svg -t 10000 && exit 1
  [[ -f "$HOME/.config/bin/driveDOWN.FLAG" ]] && /usr/bin/notify-send "driveDOWN" "En ejecución... No se puede apagar el equipo." -i /usr/share/ies/dock/icons/google_drive.svg -t 10000 && exit 1

  gxmessage Elige una opción de apagado -center -title "Elige una opción" -font "Sans bold 10" -default "Cancelar" -buttons "_Cancelar":1,"_Apagar Después":2,"_Cerrar Sesión":3,"_Reiniciar":4,"_Apagar":5 >/dev/null
  case $? in
    1)
       echo "Exit";;
    2)
       bash apagar_tiempo.sh;;
    3)
       lxterminal -e drive.sh UP $0 && openbox --exit;;
    4)
       lxterminal -e drive.sh UP $0 && sudo /sbin/shutdown -r now;;
    5)
       lxterminal -e drive.sh UP $0 && sudo /sbin/shutdown -h now;;
  esac

else

  gxmessage Elige una opción de apagado -center -title "Elige una opción" -font "Sans bold 10" -default "Cancelar" -buttons "_Cancelar":1,"_Apagar Después":2,"_Cerrar Sesión":3,"_Reiniciar":4,"_Apagar":5 >/dev/null
  case $? in
    1)
       echo "Exit";;
    2)
       bash apagar_tiempo.sh;;
    3)
       openbox --exit;;
    4)
       sudo /sbin/shutdown -r now;;
    5)
       sudo /sbin/shutdown -h now;;
  esac

fi